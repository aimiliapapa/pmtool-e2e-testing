######Libraries Used:

-pip install robotframework\
-pip install robotframework-seleniumlibrary

######IDE Used:

-PyCharm Community (https://www.jetbrains.com/pycharm/download/#section=windows)


######OS:

-Windows 10


---------------------------------------------------------------------------------

To run a test suite: robot TestSuiteName.robot\
e.g. :
```
robot logIn.robot

```

To run a single test case: robot -t "TestCaseName" TestSuiteName.robot\
e.g. :
```
robot -t "Sign Up New User" projects.robot
```

To run multiple test suites serially: robot TestSuiteName1.robot TestSuiteName2.robot	TestSuiteName3.robot etc\
e.g. :
```
robot signUp.robot logIn.robot projects.robot taskDB.robot settings.robot

```

In order to store the output log files of each test run in a specific directory (otherwise, logs will be stored in the current directory you run your tests from) , you should use --outputdir DirectoryPath\
e.g. :
```
robot --outputdir ../Logs signUp.robot logIn.robot projects.robot taskDB.robot settings.robot logout.robot
```

##NOTE1: 
If I want to run all of the suites serially I try to use this order so a common user's flow can be triggered:
```
robot --outputdir ../Logs signUp.robot logIn.robot projects.robot taskDB.robot settings.robot logout.robot

```
##NOTE2: 
You can see the log file inside the Logs folder for more info and statistics of the run :)

--------------------------------------------------------------------------------------------------------

######Few words about Robot Framework:

Robot Framework is a generic test automation framework for acceptance testing and acceptance test-driven development (ATDD). It is a keyword-driven testing framework that uses tabular test data syntax.
