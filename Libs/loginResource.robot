*** Settings ***
Resource  resource.robot

*** Keywords ***
Click Login
    Click Element  id:login

Click Login Button
     Click Button  xpath=//*[contains(text(),'Login')]
#OR  Click Button   name:action

Successful Login
    Wait Until Page Contains    Dashboard

Insert Invalid User's Email
    Input Text  id:email    ${INVLD_EMAIL}

Insert Invalid User's Password
    Input Text  id:password     ${INVLD_PASSWORD}

Unsuccessful Login Message for Email
    Wait Until Element Is Visible  xpath=//input[@id="email"]//following::div[1]/div/p
#OR Wait Until Page Contains    Invalid login info
#OR Wait Until Element Is Visible  class:invalid-feedback

Unsuccessful Login Message for Password
    Wait Until Element Is Visible  xpath=//input[@id="password"]//following::p
#OR Wait Until Page Contains    Invalid login info
#OR Wait Until Element Is Visible  class:invalid-feedback
