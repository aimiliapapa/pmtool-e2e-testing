*** Settings ***
Resource  resource.robot

*** Keywords ***
Click Logout Button
    Wait Until Element Is Visible  id:logout
    Click Element  id:logout

Assert That User Is Logged Out
    Wait Until Page Contains Element  id:welcome_card