*** Settings ***
Library  SeleniumLibrary
Library  BuiltIn
Resource  ./loginResource.robot
Resource  ./signupResource.robot
Resource  ./projectsResource.robot
Resource  ./taskdbResource.robot
Resource  ./settingsResource.robot
Resource  ./logoutResource.robot

*** Variables ***
${BROWSER}      Chrome
${SITE_URL}     https://node-fs-app.herokuapp.com/
${USER_NAME}    John Doe
${USER_EMAIL}   y2438@vcache.com
${UPDATED_NAME}     Emily Papadourou
${UPDATED_EMAIL}        yi2439@cache.com
${USER_PASSWORD}    myPassword123!
${UPDATED_PASSWORD}     newPassword123!
${USER_COMPANY}     Workable
${USER_ADDRESS}     446 Central Avenue
${INVLD_EMAIL}      invalidmailmail.com
${INVLD_PASSWORD}   InVaLidPaSs!
${CREATE_BTN_TXT}   Create
${PROJECT_NAME}     My Project
${PROJECT_DESC}     Emily's Project
${UPDATED_PROJECT_NAME}     Workable's Project
${TASK_SUMMARY}     My First Task
${UPDATED_TASK_SUMMARY}    My Second Task
${TASK_DESC}        Emily's Task
${UPDATED_TASK_DESC}     Workable Task
${TASK_STATUS}      IN REVIEW


*** Keywords ***
#####GENERAL KEYWORDS FOR TEST SET UP######
Visit Website
    Open Browser  ${SITE_URL}   ${BROWSER}
    Maximize Browser Window

Log In User Before Test
    Visit Website
    Click Login
    Insert User's Email
    Insert User's Password
    Click Login Button

Log In User With Updated Credentials Before Test
    Visit Website
    Click Login
    Insert User's Updated Email
    Insert User's Updated Password
    Click Login Button

#####GENERAL KEYWORDS FOR USER SIGNUP/LOGIN#####


Insert User's Email
    Input Text  id:email        ${USER_EMAIL}

Insert User's Password
    Input Text  id:password     ${USER_PASSWORD}










