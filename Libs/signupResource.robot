*** Settings ***
Resource  resource.robot

*** Keywords ***
Click Sign Up
    Click Element  id:signup

Click Sign Up Button
    Click Button   xpath=//*[contains(text(),'Sign Up')]
#OR Click Button    name:action

Insert User's Full Name
    Input Text  id:fullName     ${USER_NAME}

Insert User's Company
    Input Text  id:company      ${USER_COMPANY}

Insert User's Address
    Input Text  id:address      ${USER_ADDRESS}

Successful SignUp Message
    Wait Until Page Contains  Successfull registration, login to start using PPMTool

Unsuccessful Signup Message
    Wait Until Page Contains   Email `${USER_EMAIL}` already exits
#OR Wait Until Element Is Visible  class:invalid-feedback

Unsuccessful SignUp Message for Name
    Wait Until Element Is Visible  xpath=//input[@id="fullName"]//following::div[1]/div/p
#OR Wait Until Page Contains   This field is required

Unsuccessful SignUp Message for Email
    Wait Until Element Is Visible  xpath=//input[@id="email"]//following::p
#OR Wait Until Page Contains   This field is required

Unsuccessful SignUp Message For Password
    Wait Until Element Is Visible  xpath=//input[@id="password"]//following::p
#OR Wait Until Page Contains   This field is required

Unsuccessful SignUp Message For Invalid Email
     Wait Until Element Is Visible  xpath=//input[@id="email"]//following::p
#OR  Wait Until Page Contains  Invalid email format
