*** Settings ***
Resource  resource.robot

*** Keywords ***
Click Settings
    Wait Until Element Is Visible  id:settings
    Click Element  id:settings

Delete User's Name
    Press Keys  id:fullName  CTRL+a+BACKSPACE

Delete User's Email
    Press Keys  id:email  CTRL+a+BACKSPACE

Delete User's Password
    Press Keys  id:password  CTRL+a+BACKSPACE

Insert User's New Name
    Input Text  id:fullName     ${UPDATED_NAME}

Insert User's New Password
    Input Text  id:password    ${UPDATED_PASSWORD}

Insert User's New Email
    Input Text  id:email     ${UPDATED_EMAIL}
Click Updated Button
     Click Element  id:update_info

Assert That User's Info Are Updated
    Wait Until Page Contains  User info updated successfully!

Unsuccessful Update Message For Name
     Wait Until Page Contains Element      xpath=//input[@id="fullName"]//following::p

Unsuccessful Update Message For Email
    Wait Until Element Is Visible       xpath=//input[@id="email"]//following::p

Insert User's Updated Email
    Input Text  id:email        ${UPDATED_EMAIL}

Insert User's Updated Password
    Input Text   id:password   ${UPDATED_PASSWORD}
