*** Settings ***
Resource  resource.robot

*** Keywords ***
Click Dashboard
    Double Click Element  id:dashboard

Click TaskDB Button
    Wait Until Element Is Visible  id:task_db
    Double Click Element   id:task_db

Insert Updated Task Summary
     Input Text   id:summary      ${UPDATED_TASK_SUMMARY}

Insert Updated Task Description
     Input Text   id:description   ${UPDATED_TASK_DESC}

Click Sort By Summary Button
     Wait Until Element Is Visible  id:sort_tasks
     Click Element  id:sort_tasks

Check Unsorted Items
    ${username_list}=   Create List                        //creates an empty list
    @{get_name}=   Get WebElements     //div[@id='items']
    FOR    ${each}     IN      @{get_name}
    ${get_username}=  Get Text    ${each}
    END
    Append To List  ${username_list}  ${get_username}   //pushes data into list in iteration

Check Sorted Items
    ${username_list2}=  Create List                        //creates an empty list
    @{get_name2}=   Get WebElements     //div[@id='items']
    FOR    ${each}     IN      @{get_name2}
    ${get_username2}=  Get Text    ${each}
    END
    Append To List  ${username_list2}  ${get_username2}   //pushes data into list in iteration

Assert That Items Are Sorted
    Should Not Be Equal  ${username_list}   ${username_list2}

Click Search Bar
    Click Element  id:search

Enter Task's Name
    Input Text  id:search   ${TASK_SUMMARY}
