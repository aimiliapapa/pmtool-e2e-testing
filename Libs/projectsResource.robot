*** Settings ***
Resource  resource.robot

*** Keywords ***
Click Create Button
    Wait Until Element Is Visible  xpath=//*[contains(text(),'${CREATE_BTN_TXT}')]
    Click Element  xpath=//*[contains(text(),'${CREATE_BTN_TXT}')]

Insert Project's Name
    Input Text  id:name     ${PROJECT_NAME}

Insert Project's Description
    Input Text  id:description  ${PROJECT_DESC}

Unsuccessful Create Message For Name
    Wait Until Element Is Visible  xpath=//input[@id="name"]//following::p
#OR Wait Until Page Contains   This field is required

Unsuccessful Create Message For Description
    Wait Until Element Is Visible  xpath=//input[@id="description"]//following::p
#OR Wait Until Page Contains   This field is required

Click Edit Project Button
    Wait Until Element Is Visible  id:btn_update_project
    Click Element  id:btn_update_project

Update Project's Name
    Press Keys  id=name   CTRL+a+BACKSPACE
    Input Text  id:name  ${UPDATED_PROJECT_NAME}

Assert That Updated Project Exists
    Wait Until Page Contains  ${UPDATED_PROJECT_NAME}

Click Add Tasks Button
    Wait Until Element Is Visible  id:btn_add_task
    Click Element  id:btn_add_task

Insert Task Summary
    Input Text  id:summary  ${TASK_SUMMARY}

Insert Task Description
    Input Text  id:description  ${TASK_DESC}

Choose Status
    Click Element  xpath=//*[@id="root"]/div/div/div/form/div[3]/div/div/input
    Click Element  xpath=//*[contains(text(),'${TASK_STATUS}')]

Choose Labels
    Click Element  id:search_input
    Click Element  xpath=//*[contains(text(),'frontend')]

Upload File
# Put your own path to file and un-comment the step in the Test Case
    Choose File  id:attachments     C:\\Users\\emily\\testfile.txt

Assert that Task Exists
    Wait Until Page Contains  ${TASK_SUMMARY}

Click View Tasks Button
    Wait Until Element Is Visible  id:btn_view_tasks
    Click Element  id:btn_view_tasks

Click Edit Tasks Button
    Click Element   id:btn_update_task

Update Task Description
    Press Keys  id:description   CTRL+a+BACKSPACE
    Input Text  id:description  ${UPDATED_TASK_DESC}

Update Task Summary
    Press Keys  id:summary  CTRL+a+BACKSPACE
    Input Text  id:summary  ${UPDATED_TASK_SUMMARY}

Click Update Button
    Click Element  xpath=//*[contains(text(),'Update')]

Assert That Updated Task Exists
    Wait Until Page Contains  ${UPDATED_TASK_DESC}
    Wait Until Page Contains  ${UPDATED_TASK_DESC}

Drag and Drop Task
    Wait Until Page Contains    ${UPDATED_TASK_SUMMARY}
    Drag And Drop   xpath=//*[contains(text(),'${UPDATED_TASK_SUMMARY}')]   id:in_progress_items
# Keyword doesn't work correctly even after editing the source code. Task is never dropped.

Click Delete Task Button
    Wait Until Element Is Visible  id:btn_delete_task
    Click Element  id:btn_delete_task

Confirm Delete Task
    Alert Should Be Present  Are you sure you want to delete this task?     ACCEPT

Assert That Task is Deleted
    Wait Until Page Does Not Contain     ${TASK_SUMMARY}

Unsuccessful Create Message For Summary
    Wait Until Element Is Visible  xpath=//input[@id="summary"]//following::p
    #OR Wait Until Page Contains   This field is required

Unsuccessful Create Message For Task Description
    Wait Until Element Is Visible  xpath=//textarea[@id="description"]//following::p
    #OR Wait Until Page Contains   This field is required

Click Delete Project Button
    Wait Until Element Is Visible   id:delete_project
    Click Element  id:delete_project

Confirm Delete Project
    Alert Should Be Present  Are you sure you want to delete this project?     ACCEPT

Assert That Project is Deleted
    Wait Until Page Contains  There are no projects created yet.