*** Settings ***
Resource  ../Libs/resource.robot
Test Setup    Visit Website
Test Teardown  Close Browser

*** Test Cases ***
Sign Up New User
    Click Sign Up
    Insert User's Full Name
    Insert User's Email
    Insert User's Password
    Insert User's Company
    Insert User's Address
    Click Sign Up Button
    Successful SignUp Message

Sign Up Existing User
    Click Sign Up
    Insert User's Full Name
    Insert User's Email
    Insert User's Password
    Insert User's Company
    Insert User's Address
    Click Sign Up Button
    Unsuccessful SignUp Message

Sign Up User Without Name
    Click Sign Up
    Insert User's Email
    Insert User's Password
    Insert User's Company
    Insert User's Address
    Click Sign Up Button
    Unsuccessful SignUp Message For Name

Sign Up User Without Email
    Click Sign Up
    Insert User's Full Name
    Insert User's Password
    Insert User's Company
    Insert User's Address
    Click Sign Up Button
    Unsuccessful SignUp Message For Email

Sign Up User Without Password
    Click Sign Up
    Insert User's Full Name
    Insert User's Email
    Insert User's Company
    Insert User's Address
    Click Sign Up Button
    Unsuccessful SignUp Message For Password

Sign Up User With Invalid Email Format
    Click Sign Up
    Insert User's Full Name
    Insert Invalid User's Email
    Insert User's Password
    Insert User's Company
    Insert User's Address
    Click Sign Up Button
    Unsuccessful SignUp Message For Invalid Email

Sign Up User Without Credentials
    Click Sign Up
    Click Sign Up Button
    Unsuccessful SignUp Message For Name
    Unsuccessful SignUp Message For Email
    Unsuccessful SignUp Message For Password
