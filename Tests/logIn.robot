*** Settings ***
Resource  ../Libs/resource.robot
Test Setup    Visit Website
Test Teardown  Close Browser

*** Test Cases ***
Log In Valid User
    Click Login
    Insert User's Email
    Insert User's Password
    CLick Login Button
    Successful Login

Log In Invalid User
    Click Login
    Insert Invalid User's Email
    Insert Invalid User's Password
    Click Login Button
    Unsuccessful Login Message For Email
    Unsuccessful Login Message For Password

Log In Without Email
    Click Login
    Insert User's Password
    Click Login Button
    Unsuccessful Login Message For Email

Log In Without Password
    Click Login
    Insert User's Email
    Click Login Button
    Unsuccessful Login Message For Password

Log In Without Credentials
    Click Login
    Click Login Button
    Unsuccessful Login Message For Email
    Unsuccessful Login Message For Password