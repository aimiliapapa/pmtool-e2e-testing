*** Settings ***
Resource  ../Libs/resource.robot
Test Setup    Log In User Before Test
Test Teardown  Close Browser

*** Test Cases ***
Log Out User
    Click Logout Button
    Assert That User Is Logged Out