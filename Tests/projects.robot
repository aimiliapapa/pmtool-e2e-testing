*** Settings ***
Resource  ../Libs/resource.robot
Test Setup    Log In User Before Test
Test Teardown  Close Browser

*** Test Cases ***
Create Project In Dashboard
    Click Create Button
    Insert Project's Name
    Insert Project's Description
    Click Create Button

Create Project With No Name
    Click Create Button
    Insert Project's Description
    Click Create Button
    Unsuccessful Create Message For Name

Create Project With No Description
    Click Create Button
    Insert Project's Name
    Click Create Button
    Unsuccessful Create Message For Description

Create Project With No Info
    Click Create Button
    Click Create Button
    Unsuccessful Create Message For Name
    Unsuccessful Create Message For Description

Edit Project
    Click Edit Project Button
    Update Project's Name
    Click Update Button
    Assert That Updated Project Exists

Create Task in Project
    Click Add Tasks Button
    Insert Task Summary
    Insert Task Description
    Choose Status
    Choose Labels
#    Upload File
    Click Create Button
    Assert That Task Exists

Create Task With No Summary
    Click Add Tasks Button
    Insert Task Description
    Choose Status
    Choose Labels
#    Upload File
    Click Create Button
    Unsuccessful Create Message For Summary

Create Task With No Description
    Click Add Tasks Button
    Insert Task Summary
    Choose Status
    Choose Labels
#    Upload File
    Click Create Button
    Unsuccessful Create Message For Task Description

Create Task With No Summary And No Description
    Click Add Tasks Button
    Choose Status
    Choose Labels
#    Upload File
    Click Create Button
    Unsuccessful Create Message For Summary
    Unsuccessful Create Message For Task Description

View And Edit Tasks
    Click View Tasks Button
    Assert That Task Exists
    Click Edit Tasks Button
    Update Task Description
    Update Task Summary
    Click Update Button
    Assert That Updated Task Exists

Drag and Drop Tasks
    Click View Tasks Button
#    Drag And Drop Task

Delete Task
    Click View Tasks Button
    Click Delete Task Button
    Confirm Delete Task
    Assert That Task Is Deleted

Delete Project
    Click Delete Project Button
    Confirm Delete Project
    Assert That Project Is Deleted