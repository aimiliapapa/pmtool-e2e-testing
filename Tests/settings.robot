*** Settings ***
Resource  ../Libs/resource.robot
Test Teardown  Close Browser

*** Test Cases ***
Update User's Info With No Name
    Log In User Before Test
    Click Settings
    Delete User's Name
    Delete User's Email
    Insert User's New Email
    Click Update Button
    Unsuccessful Update Message For Name

Update User's Info With No Email
    Log In User Before Test
    Click Settings
    Delete User's Name
    Insert User's New Name
    Delete User's Email
    Click Update Button
    Unsuccessful Update Message For Email

Update User's Info With No Info
    Log In User Before Test
    Click Settings
    Delete User's Name
    Delete User's Email
    Click Update Button
    Unsuccessful Update Message For Name
    Unsuccessful Update Message For Email


Update User's Info
    Log In User Before Test
    Click Settings
    Delete User's Name
    Insert User's New Name
    Delete User's Email
    Insert User's New Email
    Delete User's Password
    Insert User's New Password
    Click Update Button
    Assert That User's Info Are Updated


Assert New Credentials And Re-Enter Old
    Log In User With Updated Credentials Before Test
    Click Settings
    Delete User's Name
    Insert User's Full Name
    Delete User's Email
    Insert User's Email
    Delete User's Password
    Insert User's Password
    Click Update Button
    Assert That User's Info Are Updated
