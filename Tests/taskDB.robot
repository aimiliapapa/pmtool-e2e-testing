*** Settings ***
Resource  ../Libs/resource.robot
Suite Setup      Log In User Before Test
Suite Teardown  Close Browser
Library  Collections

*** Test Cases ***
Create Project and First Task
    Click Create Button
    Insert Project's Name
    Insert Project's Description
    Click Create Button
    Click Add Tasks Button
    Insert Task Summary
    Insert Task Description
    Choose Status
    Choose Labels
#    Upload File
    Click Create Button

Create Second Task
    Click Dashboard
    Click Add Tasks Button
    Insert Updated Task Summary
    Insert Updated Task Description
    Choose Status
    Choose Labels
#    Upload File
    Click Create Button


View Tasks and Sort By Summary
    Click TaskDB Button
    Assert That Task Exists
    Assert That Updated Task Exists
    Check Unsorted Items
    Click Sort By Summary Button
    Check Sorted Items
#    Assert That Items Are Sorted

Search Task
    Click TaskDB Button
    Click Search Bar
    Enter Task's Name
    Assert That Task Exists

